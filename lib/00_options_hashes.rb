def transmogrify(string, options = {})
    defaults = {:times => 1, :upcase => false, :reverse => false}
    new = defaults.merge(options)
    new_string = string * new[:times]
    new_string.reverse! if new[:reverse] == true
    new_string.upcase! if new[:upcase] == true

end
